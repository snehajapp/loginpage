import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepass',
  templateUrl: './changepass.component.html',
  styleUrls: ['./changepass.component.css']
})
export class ChangepassComponent implements OnInit {
  changepass: boolean;

  constructor(private router: Router) { }

  ngOnInit() {
  }
  navigateTo(pageName) {
    this.changepass = true;
    this.router.navigateByUrl(pageName);
  }
}
