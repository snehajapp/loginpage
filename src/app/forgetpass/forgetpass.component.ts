import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgetpass',
  templateUrl: './forgetpass.component.html',
  styleUrls: ['./forgetpass.component.css']
})
export class ForgetpassComponent implements OnInit {
  forgetpass: boolean;
  
  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateTo(pageName) {
    this.forgetpass = true;
    this.router.navigateByUrl(pageName);
  }
}
