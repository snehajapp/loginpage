import { Component, OnInit } from '@angular/core';
import { getMaxListeners } from 'cluster';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  empStatusRecord = {
    active: 10,
    inactive:12,
    activeEmp: 2222,
    inactiveEmp: 1111
  }



  empData = [
    {
      name: 'John',
      expDate: '27 March',
      index1: 68,
      index2: 56,
      industry: 'IT',
      emailId: 'john@gmail.com.com'
    },

    {
      name: 'Wella',
      expDate: '27 March',
      index1: 68,
      index2: 56,
      industry: 'IT',
      emailId: 'john@gmail.com.com'
    },

    {
      name: 'Merry',
      expDate: '28 March',
      index1: 68,
      index2: 56,
      industry: 'IT',
      emailId: 'merry@gmail.com.com'
    },

    {
      name: 'William',
      expDate: '27 March',
      index1: 68,
      index2: 56,
      industry: 'IT',
      emailId: 'john@gmail.com.com'
    },

    {
      name: 'Stefen',
      expDate: '27 March',
      index1: 68,
      index2: 56,
      industry: 'IT',
      emailId: 'stef@gmail.com.com'
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}
